<?php

namespace Drupal\vb_event\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityDisplayRepository;

/**
 * Provides a VB Event Admin config form.
 */
class VBEventAdminConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vb_event_admin_config_form';
  }

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'vb_event.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('vb_event.settings');

    $entity_display = \Drupal::service('entity_display.repository');
    $view_modes = $entity_display->getViewModeOptionsByBundle('node', 'event');

    $form['overview_display'] = [
      '#type' => 'select',
      '#title' => $this->t('Overview viewmode'),
      '#options' => $view_modes,
      '#default_value' => $config->get('overview_display') ? $config->get('overview_display') : 'teaser',
      '#description' => $this->t('Viewmode used in the overview. The viewmode must be enabled as view display for the Event content type to be shown here.')
    ];

    $form['categories_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable event categories'),
      '#default_value' => $config->get('categories_enabled'),
      '#description' => $this->t('Enabling categories allows the user to select one or more categories per article.')
    ];

    $form['categories_exposed_filter'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show category filter on overview'),
      '#default_value' => $config->get('categories_exposed_filter'),
      '#states' => [
        'visible' => [
          ':input[name="categories_enabled"]' => ['checked' => true],
        ],
      ],
      '#description' => $this->t('Use the above categories as filters on the overview. This also adds links to the rendered categories on the detail page to the filtered overview.')
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->configFactory->getEditable('vb_event.settings')
      ->set('overview_display', $values['overview_display'])
      ->set('categories_enabled', $values['categories_enabled'])
      ->set('categories_exposed_filter', $values['categories_exposed_filter'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
